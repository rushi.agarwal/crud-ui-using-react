import React from 'react'
import axios from 'axios';

export class Update extends React.Component{

    state = {
        user_id: '',
        user_field:'',
        user_value:'',
        success_message:'',
        error_message:'',
        error: {}
    }

    onValueChange = (e) => {
        this.setState({[e.target.name]: e.target.value});
    };

    updateDetails = (e)=>{
        e.preventDefault();

        const data = new FormData();
        data.append('user_id', this.state.user_id);
        data.append('user_field', this.state.user_field);
        data.append('user_value', this.state.user_value);

        if(this.validateDetails()){
            axios.post('http://localhost:8000/update', data)
            .then((response) =>{
                console.log(response.data.message)
                this.setState({success_message: response.data.message, error_message : ""})
            })

            .catch((error) => {
                this.setState({error_message : error.response.data.message, success_message : ""})
            })
        }

        
    };


    validateDetails = () =>{
        let temp = true;
        let tempErr = {};

        if(!(this.state.user_id && this.state.user_field && this.state.user_value)){
            tempErr["empty"] = "Field cannot be empty";
            temp = false;
        }
        else if(!/^[0-9]*$/i.test(this.state.user_id)){
            tempErr["id"] = "Id must be integer";
            temp = false;
        }
        else if(! /^[a-zA-Z_]+$/i.test(this.state.user_field)){
            tempErr["field"] = "Field can have only alphabets amd underscore."
            temp = false;

        }
        else{
           tempErr["empty"] = "";
           tempErr["field"] = "";
           tempErr["id"] = "";
            temp =true;
        }
        this.setState({error: tempErr});
        return temp;
    }


    render(){
        return(
            <div className = "container">
                <h1>Update Data :)</h1>
                {this.state.error_message ? 
                    <p className = "text-danger">{this.state.error_message}</p> :
                <p className = "text-success">{this.state.success_message}</p>
                }
                <form onSubmit = {this.updateDetails}>
                    <div className="form-group">
                        <label htmlFor="user_id">ID:</label>
                        <input type="text" className ="form-control" placeholder="Enter Id for update" name ="user_id" onChange = {this.onValueChange}/>
                        <p className = "text-danger">{this.state.error.id}</p>
                    </div>   

                    <div className ="form-group">
                        <label htmlFor="user_field">Field Name:</label>
                        <input type="text" className ="form-control" placeholder="Enter the field name for update" name ="user_field" onChange = {this.onValueChange} />
                        <p className = "text-danger">{this.state.error.field}</p>
                    </div>   
          
                    <div className ="form-group">
                        <label htmlFor="user_value">Value:</label>
                        <input type="text" className ="form-control" placeholder="Enter the value for update" name ="user_value" onChange = {this.onValueChange}/>
                    </div> 

                    <p className = "text-danger">{this.state.error.empty}</p>
                    <div className = "btn-group">
                        <button type="submit" className = "btn btn-primary mr-1">Update</button>
                    </div>
                    

                    <div className = "btn-group">
                        <a className ="btn btn-primary" href="home">Home</a>
                    </div>
        
                </form>

                
            </div>
        );
    }
}