import React from 'react';
import axios from 'axios';

export class Read extends React.Component{
    state = {
        read_id: '',
        error:'',
        dataList: []
    }

    onValueChange = (e) => {
        this.setState({[e.target.name]: e.target.value});
    };


    fetchDetails = (e)=>{
        e.preventDefault();

        const data = new FormData();
        data.append('read_id', this.state.read_id);

        if(this.validateDetails()){
            axios.post('http://localhost:8000/read', data)
            .then((response) =>{
                console.log(response.data.message)
                this.setState({dataList: [...response.data.message]})
            })

            .catch((error) => {
                this.setState({error : error.response.data.message})
            })
        }

        
    };

    validateDetails = () =>{
        let temp = true;
        let message = '';

        if(!(this.state.read_id)){
            message = "Field cannot be empty";
            temp = false
        }
        else if(!/^[0-9]*$/i.test(this.state.read_id)){
            message = "Id must be integer";
            temp = false;
        }
        else{
            message = "";
            temp =true;
        }
        this.setState({error: message});
        return temp;
    }

    renderRow = () =>{
        return(
            this.state.dataList.map(function(dataItem,i){
                return(
                    <tr key = {dataItem.user_id}> 
                        <td key = {dataItem.user_id}>{dataItem.user_id}</td>
                        <td key = {dataItem.user_name}>{dataItem.user_name}</td>
                        <td key = {dataItem.user_age}>{dataItem.user_age}</td>
                        <td key = {dataItem.user_interest}>{dataItem.user_interests}</td>
                    </tr>
                );
                
            })
        );
    }

    render(){
        return(
        <div className = "container">
            <h1>We are happy to fetch your data :)</h1>
            <p className = "text-danger">{this.state.error}</p> 
        <p>{this.state.data}</p>
            <form onSubmit = {this.fetchDetails}>
                <div className ="form-group">
                    <label htmlFor="read_id">ID:</label>
                    <input type="text" className ="form-control" placeholder="Enter your ID" name="read_id" onChange = {this.onValueChange} />
                </div>   
                <button type="submit" className = "btn btn-primary">Fetch</button>
                <br /><br />
            </form>
            <table className ="table table-striped">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Age</th>
                    <th>Interests</th>
                </tr>
                </thead>
                <tbody>
                   {this.renderRow()}                
                </tbody>
            </table>
            
            <br /> <br />
            <a className ="btn btn-primary" href="home">Home</a>
        </div>
        );
    }
}
