import React from 'react';
import axios from 'axios';


export class Add extends React.Component{
    state = {
        user_id: '',
        user_name: '',
        user_age: '',
        user_interests: '',
        success_message:'',
        error_message:'',
        error:{}
    };

    onValueChange = (e) => {
        this.setState({[e.target.name]: e.target.value});
    };

    onHandleSubmit = (e)=> {
        e.preventDefault();

        if(this.validateFields()){

            const data = new FormData();
            data.append('user_id', this.state.user_id);
            data.append('user_name', this.state.user_name);
            data.append('user_age', this.state.user_age);
            data.append('user_interests', this.state.user_interests);

            axios.post("http://localhost:8000/create", data)
                .then((response) => {
                    this.setState({success_message: response.data.message,
                                   error_message: ""})
                })
                .catch((error)=>{   
                    this.setState({error_message: error.response.data.message, success_message: ""})
                })   
        }
    };

    validateFields = () => {

        let temp = true;
        let tempErr = {};

        if(!(this.state.user_name && this.state.user_id && this.state.user_age && this.state.user_interests)){
            tempErr["empty"] = "Field cannot be empty";
            temp = false;
        }
        else if(! /^[a-zA-Z ]+$/i.test(this.state.user_name)){
            tempErr["name"] = "Name can have only alphabets amd space."
            temp = false;

        }
        else if(!/^[a-zA-Z, ]+$/i.test(this.state.user_interests)){
            tempErr["interest"] = "Interests can have only alphabets, space and comma."
            temp = false;
        }
        else if(!/^[0-9]*$/i.test(this.state.user_id)){
            tempErr["id"] = "Id must be integer";
            temp = false;
        }
        else if(!/^[0-9]*$/i.test(this.state.user_age) || this.state.user_age > 60){
            tempErr["age"] = "Age must be integer and less than or equal to 60";
            temp = false;
        }
        else{
            tempErr["empty"] = "";
            tempErr["name"] = "";
            tempErr["interest"] = "";
            tempErr["id"] = "";
            tempErr["age"] = "";
        }
        this.setState({error : tempErr});

        return temp;
    };



    render(){
        return(
            <div className = "container">
                <h1 className = "mt-4 mb-5">Register yourself :)</h1>
                {this.state.success_message ? 
                <p className = "text-success">{this.state.success_message}</p> :
                <p className = "text-danger">{this.state.error_message}</p>}
                
                <form onSubmit = {this.onHandleSubmit}>
    
                <div className="form-group">
                    <label htmlFor="user_id">ID:</label>
                    <input type="text" className="form-control" placeholder="Enter Id" name="user_id" onChange={this.onValueChange} />
                    <p className = "text-danger">{this.state.error.id}</p>
                </div>    
    
                <div className="form-group">
                    <label htmlFor="user_name">Name:</label>
                    <input type="text" className="form-control" placeholder="Enter Name" name ="user_name" onChange={this.onValueChange} />
                    <p className = "text-danger">{this.state.error.name}</p>
                </div>        
    
                <div className="form-group">
                    <label htmlFor="user_age">Age:</label>
                    <input type="text" className = "form-control" name = "user_age" placeholder="Enter Age" onChange={this.onValueChange}/>
                    <p className = "text-danger">{this.state.error.age}</p>
                </div>
    
                <div className="form-group">
                    <label htmlFor="user_interests">Interests:</label>
                    <input type="text" className = "form-control" name = "user_interests" placeholder="Enter Interests" onChange={this.onValueChange} />
                    <p className = "text-danger">{this.state.error.interest}</p>
                </div>
                <p className = "text-danger">{this.state.error.empty}</p>

                <div className = "btn-group">
                    <button type="submit" className = "btn btn-primary mr-1">Add</button>
                </div>
                <div className = "btn-group">
                    <a href = "home" className="btn btn-primary">Home</a>
                </div>
            </form>

            
        </div>
        );
    }
}