import React from 'react'
import axios from 'axios';

export class Delete extends React.Component{

    state = {
        user_id: '',
        success_message:'',
        error_message:'',
        error: {}
    }

    onValueChange = (e) => {
        this.setState({[e.target.name]: e.target.value});
    };


    deleteDetails = (e)=>{
        e.preventDefault();

        const data = new FormData();
        data.append('user_id', this.state.user_id);

        if(this.validateDetails()){
            axios.post('http://localhost:8000/delete', data)
            .then((response) =>{
                console.log(response.data.message)
                this.setState({success_message: response.data.message, error_message : ""})
            })

            .catch((error) => {
                this.setState({error_message : error.response.data.message, success_message : ""})
            })
        }

        
    };

    validateDetails = () =>{
        let temp = true;
        let tempErr = {};

        if(!(this.state.user_id)){
            tempErr["empty"] = "Field cannot be empty";
            temp = false;
        }
        else if(!/^[0-9]*$/i.test(this.state.user_id)){
            tempErr["id"] = "Id must be integer";
            temp = false;
        }
        else{
           tempErr["empty"] = "";
           tempErr["id"] = "";
            temp =true;
        }
        this.setState({error: tempErr});
        return temp;
    }

    render(){
        return(
            <div className = "container">
                <h1>Delete Data :(</h1>
                {this.state.error_message ? 
                    <p className = "text-danger">{this.state.error_message}</p> :
                    <p className = "text-success">{this.state.success_message}</p>
                }
                <form onSubmit = {this.deleteDetails}>
                    <div className ="form-group">
                        <label htmlFor="user_id">ID:</label>
                        <input type="text" className="form-control" placeholder="Enter Id for deletion" name ="user_id" onChange = {this.onValueChange}/>
                        <p className = "text-danger">{this.state.error.id}</p>
                    </div>        

                    <p className = "text-danger">{this.state.error.empty}</p>
                    <div className = "btn-group">
                        <button type="submit" className = "btn btn-primary mr-1">Delete</button>
                    </div>
                
                    <div className = "btn-group">
                        <a className ="btn btn-primary" href="home">Home</a>
                    </div>
        
                </form>
            </div>
        );
    }
}