import create from '../images/create.png';
import read from '../images/read.png';
import update from '../images/update.jpg';
import deleteImage from '../images/delete.png';
import '../styles.css';
import React from 'react';

export class Options extends React.Component {

    render(){
      return (
        <div className="heading">
          <h1>Welcome to CRUD ki Duniya</h1>
          <br />
          <p className = "option">Click on any one option to perform</p>
          <br />
          <div className="image">     
            <div className = "image_column">
              <a href="create">
                <img src = {create} alt = "register" height = "100" width = "100" />
                <p>Create</p>
              </a>
                  
            </div>
            <div className = "image_column">
                <a href="read">
                    <img src = {read} alt = "read" height = "100" width = "100" />
                    <p>Read</p>
                </a>
            </div>
            <div className = "image_column">
              <a href="update">
                <img src = {update} alt = "update" height = "100" width = "100" />
                <p>Update</p>
              </a>    
            </div>
            <div className = "image_column">
              <a href="delete">
                <img src = {deleteImage} alt = "delete" height = "100" width = "100" />
                <p>Delete</p>
              </a>    
          </div>
      </div>
        </div>
      );
  
    }
}