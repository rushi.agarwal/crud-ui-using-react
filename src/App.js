import './App.css';
import React from 'react';
import {Options} from './components/Options';
import {Add} from './components/Add';
import {Read} from './components/Read';
import {Update} from './components/Update';
import {Delete} from './components/Delete';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';


class App extends React.Component {
  render(){
    return(
      <Router>
        <Switch>
        <Route path = "/home">
            <Options />
          </Route>
          <Route path = '/' exact>
            <Options />
          </Route>
          <Route path = "/create">
            <Add />
          </Route>
          <Route path = "/read">
            <Read />
          </Route>
          <Route path = "/update">
            <Update />
          </Route>
          <Route path = "/delete">
            <Delete />
          </Route>
        </Switch>
      </Router>
    );  

  }
  
}

export default App;
